requirejs.config({
	baseUrl: 'app/js',
	paths: {
		"app": './main',
		"navbar": './navbar',
		"dropdown-menu": '../libs/custom-dropdown-menu/dropdown-menu.component',
		"login-button": '../libs/login-button/login-button.component',
		"angularjs": '//ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min',
		"jquery": '//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min',
		"bootstrap": '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min'
	},
	shim: {
		bootstrap: {
			dep: ['jquery']
		}
	}
});

requirejs(["app", "navbar", "dropdown-menu", "login-button"]);