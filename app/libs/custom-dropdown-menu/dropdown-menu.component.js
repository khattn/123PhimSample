define(['angularjs'], function ($) {
  function dropdownMenuCtrl($window, $http) {
    this.isHideContent = true;

    $http.get("/KhaTran/123PhimSample/app/datas/locations.json").then((res) => {
      this.locations = res.data.locations;
    })

    this.onClick = function () {
      this.isHideContent = !this.isHideContent;
    }

    this.onBlur = function () {
      if (!this.isHideContent) {
        this.isHideContent = true;
      }
    }
  }

  var app = angular.module('app').component('dropdownMenu', {
    bindings: {
      isScroll: '='
    },
    templateUrl: "app/libs/custom-dropdown-menu/dropdown-menu.html",
    controller: dropdownMenuCtrl
  });
});
