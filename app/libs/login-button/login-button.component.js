define(['angularjs'], function ($) {
  function loginButtonCtrl() {
    this.login = function () {
      console.log('login');
      console.log(this.isScroll);
    }
  }

  var app = angular.module('app').component('loginButton', {
    bindings: {
      isScroll: '='
    },
    templateUrl: "app/libs/login-button/login-button.html",
    controller: loginButtonCtrl
  });
});
