define(['angularjs'], function ($) {
	function navBarController($window) {
		return function (scope, element, attrs) {
			angular.element($window).bind("scroll", function () {
				if (this.pageYOffset >= 100) {
					scope.isScroll = true;
				} else {
					scope.isScroll = false;
				}
				scope.$apply();
			});
		}
	}

	var app = angular.module('app').directive('scrollNavbar', navBarController);
});
